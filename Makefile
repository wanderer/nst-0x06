# JFLAGS = -g -verbose -Xlint:all -d ./didyousayspiderman/out
PROJECT = didyousayspiderman
OUTDIR = $(PROJECT)/out
J = java
RUNFLAGS = -classpath $(OUTDIR) $(PROJECT).crawler -u https://git.dotya.ml
JFLAGS = -g -verbose -d ./$(OUTDIR)
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

JAVA_FILES:=$(wildcard ./*/src/*/*.java)
JAVA_CLASSES:=$(patsubst %.java,%.class,$(JAVA_FILES))

CLASSES:=$(JAVA_CLASSES)

default: build

.PHONY: build clean testrun
build: $(CLASSES:.java=.class)

runtest:
	@echo [*] running a testrun
	$(J) $(RUNFLAGS)

test: build runtest clean

clean:
	@echo [*] cleaning up
	$(RM)v ./*/{out/*,src/*}/*.class
