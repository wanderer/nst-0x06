package didyousayspiderman;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.text.html.parser.*;
import javax.swing.text.html.*;
import javax.swing.text.*;

class URIinfo {
	URI uri;
	int depth;
	URIinfo(URI uri, int depth) {
		this.uri=uri;
		this.depth=depth;
	}
	URIinfo(String str, int depth) {
		try {
			this.uri=new URI(str);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.depth=depth;
	}

}

/** Trida ParserCallback je pouzivana parserem DocumentParser,
 * je implementovan primo v JDK a umi parsovat HTML do verze 3.0.
 * Pri parsovani (analyze) HTML stranky voli tento parser
 * jednotlive metody tridy ParserCallback, coz nam umoznuje
 * provadet s ��stmi HTML stranky nase vlastni akce.
 * @author Tomas Dulik
 */
class ParserCallback extends HTMLEditorKit.ParserCallback {
	/**
	 * pageURI bude obsahovat URI aktualne parsovane stranky. Budeme
	 * jej vyuzivat pro resolving vsech URL, ktere v kodu stranky najdeme
	 * - predtym, nez najdene URL ulozime do foundURLs, musime z nej udelat
	 * absolutni URL!
	 */
	URI pageURI;
	/**
	 * depth bude obsahovat aktualni hloubku zanoreni
	 */
	int depth=0, maxDepth=0;
	/** visitedURLs je mnozina vsech URL, ktere jsme jiz navstivili
	 * (parsovali). Pokud najdeme na strance URL, ktere je v teto mnozine,
	 * nebudeme jej uz dale parsovat
	 */
	HashSet<URI> visitedURIs;
	/**
	 * foundURLs jsou vsechna nova (zatim nenavstivena) URL, ktere na strance
	 * najdeme. Pote, co projdeme celou stranku, budeme z tohoto seznamu
	 * jednotlive URL brat a zpracovavat.
	 */
	LinkedList<URIinfo> foundURIs;
	/** pokud debugLevel>1, budeme vypisovat debugovaci hlasky na std. error */
	int debugLevel=0;

	ParserCallback (HashSet<URI> visitedURIs, LinkedList<URIinfo> foundURIs) {
		this.foundURIs=foundURIs;
		this.visitedURIs=visitedURIs;
	}

	/**
	 *  metoda handleSimpleTag se vola napr. u znacky <FRAME>
	 */
	public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		handleStartTag(t, a, pos);
	}

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		URI uri;
		String href=null;
		if (debugLevel>1)
			System.err.println("handleStartTag: "+t.toString()+", pos="+pos+", attribs="+a.toString());
		if (depth<=maxDepth)
			if (t==HTML.Tag.A) href=(String)a.getAttribute(HTML.Attribute.HREF);
			else if (t==HTML.Tag.FRAME) href=(String)a.getAttribute(HTML.Attribute.SRC);
			if (href!=null)
				try {
					uri = pageURI.resolve(href);
					if (!uri.isOpaque() && !visitedURIs.contains(uri)) {
						visitedURIs.add(uri);
						foundURIs.add(new URIinfo(uri,depth+1));
						if (debugLevel>0)
						 System.err.println("Adding URI: "+uri.toString());
					}
				} catch (Exception e) {
					System.err.println("Bad bad URI found: "+href);
					System.err.println(e);
				}

		}
/******************************************************************
 * V metode handleText bude probihat veskera cinnost, souvisejici se
 * zjistovanim cetnosti slov v textovem obsahu HTML stranek.
 * IMPLEMENTACE TETO METODY JE V TETO ULOZE VASIM UKOLEM !!!!
 * Mozny postup:
 * Ve tride Parser (klidne v jeji metode main) si vyrobte vyhledavaci tabulku
 * =instanci tridy HashMap<String,Integer> nebo TreeMap<String,Integer>.
 * Do teto tabulky si ukladejte dvojice klic-data, kde
 * klicem necht jsou jednotliva slova z textoveho obsahu HTML stranek,
 * data typu Integer bude dosavadni pocet vyskytu daneho slova v
 * HTML strankach.
 *******************************************************************/
	LinkedHashMap<String, Integer> lhm = new LinkedHashMap<>();
	public void handleText(char[] data, int pos) {
		System.out.println("handleText: "+String.valueOf(data)+", pos="+pos);

		String[] datastring = String.valueOf(data).split(" ");
		int obj_val = -1;
		for (int i = 0; i < datastring.length; i++){
			if (lhm.containsKey(datastring[i])){
				obj_val = lhm.get(datastring[i]);
				lhm.replace(datastring[i], obj_val, obj_val + 1);
			} else {
				lhm.putIfAbsent(datastring[i], 1);
			}
		}
	}
}

class Finding implements Comparable<Finding>{
	/* implementation inspired by https://www.javacodeexamples.com/java-linkedlist-sort-example/2390 */
	private int occurrence_count;
	private String word;

	public Finding (int occurrence_count, String word){
		this.occurrence_count = occurrence_count;
		this.word = word;
	}

	public int compareTo(Finding some_other_finding) {
		return this.occurrence_count - some_other_finding.occurrence_count;
	}

	public String toString(){
		return this.word + " => " + this.occurrence_count;
	}
}

public class crawler {
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Not enough parameters");
			return;
		}
		LinkedList<URIinfo> foundURIs=new LinkedList<URIinfo>();
		HashSet<URI> visitedURIs=new HashSet<URI>();
		URI uri;
		int debugpls = 0;
		int maxdepth = 0;
		try {
			int argsposition = 0;
			while (argsposition < args.length) {
				switch (args[argsposition]){
					case "-f":
					case "--fileurls":
						argsposition++;
						File file = new File(args[argsposition]);
						argsposition++;
						Scanner sc = new Scanner(file);
						while (sc.hasNextLine())
						{
							String nuurlpart = sc.nextLine();
							if (!nuurlpart.endsWith("/")) {nuurlpart += "/";}
							uri = new URI(nuurlpart);
							foundURIs.add(new URIinfo(uri, 0));
							visitedURIs.add(uri);
						}
						break;
					case "-v":
					case "--verbose":
						/* -v as in verbose debugging */
						debugpls = 2;
						argsposition++;
						break;
					case "-m":
					case "--maxdepth":
						argsposition++;
						maxdepth = Integer.parseInt(args[argsposition]);
						argsposition++;
						if (maxdepth < 0){
							System.err.println("Warning: \"maxdepth\" must be 0 or greater, defaulting to 0");
							maxdepth = 0;
						}
						break;
					case "-u":
					case "--urllist":
						/* pass a URL or a list of comma,separated URLs */
						argsposition++;
						String[] urls = args[argsposition].split(",");
						argsposition++;
						for (int i = 0; i < urls.length; i++){
							if (!urls[i].endsWith("/")) {urls[i] += "/";}
							uri = new URI(urls[i]);
							foundURIs.add(new URIinfo(uri, 0));
							visitedURIs.add(uri);
						}
						break;
					default:
						System.err.println("Unknown parameter " + args[argsposition]);
						argsposition += 1;
						break;
				}
			}

			ParserCallback callBack=new ParserCallback(visitedURIs, foundURIs);
			ParserDelegator parser=new ParserDelegator();

			/* set our options for the whole run */
			callBack.maxDepth = maxdepth;
			callBack.debugLevel = debugpls;
			System.out.println("* max depth: "+ callBack.maxDepth + "\n* debug level " + callBack.debugLevel);

			while (!foundURIs.isEmpty()) {
				URIinfo URIinfo=foundURIs.removeFirst();
				callBack.depth=URIinfo.depth;
				callBack.pageURI=uri=URIinfo.uri;
				System.err.println("Analyzing "+uri);
				try {
					BufferedReader reader=new BufferedReader(new InputStreamReader(uri.toURL().openStream()));
					parser.parse(reader, callBack, true);
					reader.close();
				} catch (FileNotFoundException e) {
					System.err.println("Error loading page - does it exist?");
					System.err.println(e);
				} catch (ConnectException e){
					System.err.println("Dang - connection refused");
					System.err.println(e);
				} catch (UnknownHostException e) {
					if (e.getMessage().endsWith(".onion")){
						System.err.println("This host is not on clearweb - skipping for now.");
					} else {
						System.err.println("The DNS record for this host might no longer exist.");
					}
					System.err.println(e);
				} catch (IOException e) {
					if (e.getMessage().contains("403")){
						System.err.println("This host returned a 403.");
					} else if (e.getMessage().contains("502")){
						System.err.println("This host returned a 502.");
					} else if (e.getMessage().contains("503")){
						System.err.println("This host returned a 503.");
					} else {
						System.err.println("This host returned an error.");
					}
					System.err.println(e);
				}
			}
			/* LinkedHashMap --> LinkedList && printallthegoodies */
			LinkedList<Finding> ll = new LinkedList<>();
			for( Map.Entry <String, Integer> entry : callBack.lhm.entrySet()){
				ll.add(new Finding(entry.getValue(), entry.getKey()));
			}
			ll.sort(Comparator.reverseOrder());
			System.out.println("====================================\nPrinting top 20 occurrences");
			for (int i = 0; i < 20; i++) {
				System.out.println(ll.get(i));
			}
		} catch (Exception e) {
			System.err.println("Uncaught exception, exiting...");
			e.printStackTrace();
		}
	}
}

